import {ArgumentsHost, BadRequestException, Catch, ExceptionFilter} from '@nestjs/common';
import {ValidationError} from 'class-validator';

@Catch(BadRequestException)
export default class BadRequestExceptionFilter implements ExceptionFilter {
    catch(exception: BadRequestException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest();
        console.error("[REJECTED]", exception.message.message);

        response
            .status(400)
            .json({
                statusCode: 400,
                timestamp: new Date().toISOString(),
                path: request.url,
            });
    }
}
