import {RuntimeConfig} from './runtime-config';

export const CONFIG_TOKEN = 'CONFIG_TOKEN';

export async function loadConfig(): Promise<RuntimeConfig> {
  console.log(process.env);
  return {
    dbConfig: {
      type: 'postgres',
      host: process.env.POSTGRES_SERVICE_HOST,
      port: +process.env.POSTGRES_SERVICE_PORT,
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB,
    },
    environment: 'prod',
  } as RuntimeConfig;
}

export const configProviders = [
  {
    provide: CONFIG_TOKEN,
    useFactory: async () => {
      return await loadConfig();
    },
    inject: [],
  },
];
