import {PostgresConnectionOptions} from 'typeorm/driver/postgres/PostgresConnectionOptions';


export interface DBConfig extends PostgresConnectionOptions {
  type: 'postgres';
  host: string;
  port: number;
  username: string;
  password: string;
  database: string;
  synchronize: boolean,
}


export interface RuntimeConfig {
  dbConfig: DBConfig;
  environment: 'dev' | 'prod';
}


