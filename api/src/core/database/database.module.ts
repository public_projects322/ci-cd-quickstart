import {Module} from '@nestjs/common';
import {databaseProviders} from './databas.providers';
import {ConfigModule} from '../config/config.module';
import {LoggerModule} from '../logger/logger.module';

@Module({
    providers: [
        ...databaseProviders
    ],
    imports: [
        ConfigModule,
        LoggerModule,
    ],
    exports: [
        ...databaseProviders
    ],
})
export class DatabaseModule {
}
