import {createConnection} from 'typeorm';
import {CONFIG_TOKEN} from '../config/config.providers';
import {RuntimeConfig} from '../config/runtime-config';
import {CustomLogger} from '../logger/custom-logger';
import * as path from 'path';

export const DB_CONNECTION_TOKEN = 'DbConnectionToken';
export const databaseProviders = [
  {
    provide: DB_CONNECTION_TOKEN,
    useFactory: async (config: RuntimeConfig, logger: CustomLogger) => {
      try {
        return await createConnection({
          ...config.dbConfig,
          entities: [
            __dirname + './../../**/*.entity{.ts,.js}',
          ],
          migrations: [
            path.join(__dirname, './../../migration/') + '**.js',
          ],
          migrationsRun: config.environment === 'prod',
        });
      } catch (e) {
        logger.log(e);
        throw e
      }
    },
    inject: [CONFIG_TOKEN, CustomLogger],
  },
];
