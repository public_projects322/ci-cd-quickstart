import {Example} from './example.entity';
import {DB_CONNECTION_TOKEN} from '../../core/database/databas.providers';
import {Connection} from 'typeorm';

export const EXAMPLE_REPOSITORY_TOKEN = 'EXAMPLE_REPOSITORY_TOKEN';

export const exampleRepository = {
        provide: EXAMPLE_REPOSITORY_TOKEN,
        useFactory: (connection: Connection) => {
                return connection.getRepository(Example);
        },
        inject: [DB_CONNECTION_TOKEN],
};
