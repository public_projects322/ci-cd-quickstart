import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';
import {ApiModelProperty} from '@nestjs/swagger';

@Entity()
export class Example {

  @PrimaryGeneratedColumn()
  @ApiModelProperty()
  id!: number;

  @Column({length: 500})
  @ApiModelProperty()
  name!: string;

  @Column('text')
  @ApiModelProperty()
  description!: string;

  @Column()
  @ApiModelProperty()
  filename!: string;

  @Column('int')
  @ApiModelProperty()
  views!: number;

  @Column()
  @ApiModelProperty()
  isPublished!: boolean;
}
