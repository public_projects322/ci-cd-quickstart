import {Body, Controller, Delete, Get, Param, Patch, Post} from '@nestjs/common';
import {ExampleService} from './example.service';
import {Example} from './example.entity';
import {ExampleDto} from './example.dto';
import {
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
} from '@nestjs/swagger';
import {CustomLogger} from '../../core/logger/custom-logger';

@Controller('example')
export class ExampleController {


  constructor(private exampleService: ExampleService, private logger: CustomLogger) {
  }

  @Get()
  @ApiOkResponse({type: Example, isArray: true})
  getAll(): Promise<Example[]> {
    // this.logger.log("Start")
    this.logger.log('Start');
    return this.exampleService.findAll();
  }


  @Get('/:id')
  @ApiNotFoundResponse({})
  @ApiOkResponse({type: Example})
  async show(@Param('id') id: string) {
    console.log(id);
    return await this.exampleService.byId(+id);
  }


  @Post()
  @ApiCreatedResponse({description: 'The record has been successfully created.', type: Example})
  @ApiForbiddenResponse({description: 'Forbidden.'})
  async create(@Body() example: ExampleDto) {
    const createdExample = await this.exampleService.create(example);
    return {example: createdExample};
  }

  @Patch('/:id')
  @ApiOkResponse({type: Example})
  @ApiNotFoundResponse({})
  async update(@Param('id') id: string, @Body() example: Partial<ExampleDto>) {
    return await this.exampleService.update(+id, example);
  }

  @Delete('/:id')
  @ApiOkResponse({})
  @ApiNotFoundResponse({})
  async remove(@Param('id') id: string) {
    await this.exampleService.remove(+id);
    return;
  }


}
