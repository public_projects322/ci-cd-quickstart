import {IsInt, IsNumber, IsOptional, MaxLength, MinLength} from 'class-validator';
import {ApiModelProperty} from '@nestjs/swagger';

export class ExampleDto {

    @IsOptional()
    @IsNumber()
    @IsInt()
    @ApiModelProperty()
    id!: number;


    @MinLength(0)
    @MaxLength(100)
    @ApiModelProperty()
    name!: string;


    @MinLength(0)
    @MaxLength(100)
    @ApiModelProperty()
    description!: string;


    @MinLength(0)
    @MaxLength(100)
    @ApiModelProperty()
    filename!: string;


    @MinLength(0)
    @MaxLength(100)
    @ApiModelProperty()
    views!: number;


    @MinLength(0)
    @MaxLength(100)
    @ApiModelProperty()
    isPublished!: boolean;
}
