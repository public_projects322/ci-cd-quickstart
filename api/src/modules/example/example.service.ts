import {Inject, Injectable, NotFoundException} from '@nestjs/common';
import {Repository} from 'typeorm';
import {Example} from './example.entity';
import {EXAMPLE_REPOSITORY_TOKEN} from './example.repository';

@Injectable()
export class ExampleService {

    constructor(
        @Inject(EXAMPLE_REPOSITORY_TOKEN)
        private readonly exampleRepository: Repository<Example>,
    ) {
    }

    async create(payload: Partial<Example>) {
        return this.exampleRepository.insert(payload);
    }

    async findAll(): Promise<Example[]> {
        return await this.exampleRepository.find();
    }

    async byId(id: number): Promise<Example> {
        const example = await this.exampleRepository.findOne(id);

        if (!example) {
            throw new NotFoundException(`Couldn't find record with id: ${id}'`);
        }

        return example;
    }

    async update(id: number, payload: Partial<Example>) {
        return await this.exampleRepository.update(id, payload);
    }

    async remove(id: number) {
        const example = await this.exampleRepository.findOne(id);
        if (!example) {
            throw new NotFoundException(`Couldn't find record with id: ${id}'`);
        }
        return await this.exampleRepository.remove(example);
    }
}
