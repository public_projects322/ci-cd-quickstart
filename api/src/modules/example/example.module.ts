import {Module} from '@nestjs/common';
import {ExampleController} from './example.controller';
import {ExampleService} from './example.service';
import {exampleRepository} from './example.repository';
import {DatabaseModule} from '../../core/database/database.module';
import {LoggerModule} from '../../core/logger/logger.module';

@Module({
  imports: [
    DatabaseModule,
    LoggerModule,
  ],
  controllers: [
    ExampleController,
  ],
  providers: [
    exampleRepository,
    ExampleService,
  ],
})
export class ExampleModule {
}
