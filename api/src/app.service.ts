import {Injectable} from '@nestjs/common';

@Injectable()
export class AppService {
    root(): string {
        console.log("successful hit of root");
        return 'Hello World!';
    }
}
