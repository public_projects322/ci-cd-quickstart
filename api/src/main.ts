import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {ValidationPipe} from '@nestjs/common';
import BadRequestExceptionFilter from './core/exception/bad-request.exception-filter';


async function bootstrap() {

    const app = await NestFactory.create(AppModule, {});

    app.useGlobalPipes(new ValidationPipe());
    app.useGlobalFilters(new BadRequestExceptionFilter());

    const port = process.env.PORT ? +process.env.PORT : 3000;
    await app.listen(port);
}


bootstrap().catch(console.error);
