import * as express from 'express';
import * as proxy from 'express-http-proxy';

import {join} from 'path';

const app = express();
const PORT = +process.env.PORT || 4200;
const HOST = process.env.HOST || 'localhost';
const DIST_FOLDER = join(process.cwd(), 'dist-client');
const APIPORT = +process.env.APIPORT || 4000;


app.use(express.static(join(DIST_FOLDER, 'browser')));
app.use('/api', proxy('localhost:4000'));

// Start up the Node server
app.listen(PORT, HOST, () => {
  console.log(`Node server listening on http://${HOST}:${PORT}`);
});
