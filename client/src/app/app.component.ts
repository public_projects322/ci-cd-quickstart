import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ResponsiveService} from './common/layout/responsive/responsive.service';
import {MatSidenav} from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  navMode = 'side';
  opened = true;

  @ViewChild('sidenav') sidenav!: MatSidenav;

  constructor(private httpClient: HttpClient,
              private responsiveService: ResponsiveService) {

  }


  ngOnInit() {
    this.responsive();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    if (event.target.innerWidth < 768) {
      this.navMode = 'over';
      this.sidenav.close();
    }
    if (event.target.innerWidth > 768) {
      this.navMode = 'side';
      this.sidenav.open();
    }
  }

  private responsive() {
    if (this.responsiveService.isMobile()) {
      this.navMode = 'over';
      this.opened = false;
    }
  }


}
