import {Component, OnInit} from '@angular/core';
import {ExampleService} from '../../common/exampleapi/example.service';
import {Observable} from "rxjs";

@Component({
  selector: 'app-landingpage',
  templateUrl: './landingpage.component.html',
  styleUrls: ['./landingpage.component.scss'],
})
export class LandingpageComponent implements OnInit {

  res$: Observable<string> | undefined;

  constructor(private exampleService: ExampleService) {
  }

  ngOnInit() {}

  testApiRequest() {
    this.res$ = this.exampleService.getTestResponse();
  }
}
