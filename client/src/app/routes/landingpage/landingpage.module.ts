import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LandingpageRoutingModule} from './landingpage-routing.module';
import {LandingpageComponent} from './landingpage.component';
import {MatButtonModule} from '@angular/material';

@NgModule({
  declarations: [
    LandingpageComponent,
  ],
  imports: [
    CommonModule,
    LandingpageRoutingModule,
    MatButtonModule,
  ],
})
export class LandingpageModule {
}
