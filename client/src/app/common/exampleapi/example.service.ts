import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})

export class ExampleService {

  constructor(private httpClient: HttpClient) {}

  getTestResponse(): Observable<string> {
    return this.httpClient.get<string>('/api/example');
  }
}
