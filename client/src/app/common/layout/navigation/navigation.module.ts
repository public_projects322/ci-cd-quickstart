import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavigationComponent} from './navigation.component';
import {MatIconModule, MatListModule, MatProgressSpinnerModule} from '@angular/material';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [CommonModule, MatListModule, MatIconModule, MatProgressSpinnerModule, RouterModule],
  exports: [NavigationComponent],
  declarations: [NavigationComponent],
})
export class NavigationModule {
}
