import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ResponsiveService {
  isMobile(): boolean {
    return document.body.clientWidth <= 768;
  }
}
