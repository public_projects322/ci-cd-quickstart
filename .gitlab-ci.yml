image: alpine:latest

variables:
  POSTGRES_USER: user
  POSTGRES_PASSWORD: testing-password
  POSTGRES_DB: $CI_ENVIRONMENT_SLUG
  KUBERNETES_VERSION: 1.10.9
  HELM_VERSION: 2.11.0
  DOCKER_DRIVER: overlay2

stages:
  - build
  - test
  - review
  - e2e
  - staging
  - cleanup
  - delivery

build client:
  image: docker:stable
  services:
    - docker:dind
  stage: build
  script:
    - registry_login
    - cd client
    - docker build -t "$CI_APPLICATION_REPOSITORY/client:$CI_APPLICATION_TAG" .
    - echo "Pushing to GitLab Container Registry..."
    - docker push "$CI_APPLICATION_REPOSITORY/client:$CI_APPLICATION_TAG"

build server:
  image: docker:stable
  services:
    - docker:dind
  stage: build
  script:
    - registry_login
    - cd api
    - docker build -t "$CI_APPLICATION_REPOSITORY/server:$CI_APPLICATION_TAG" .
    - echo "Pushing to GitLab Container Registry..."
    - docker push "$CI_APPLICATION_REPOSITORY/server:$CI_APPLICATION_TAG"

test client:
  stage: test
  image: trion/ng-cli-karma
  script:
    - cd client
    - npm ci
    - npm run test
  only:
    - branches
  except:
    variables:
      - $TEST_DISABLED

test server:
  stage: test
  image: node:alpine
  script:
    - cd api
    - npm ci
    - npm run test
  only:
    - branches
  except:
    variables:
      - $TEST_DISABLED


review:
  stage: review
  script:
    - install_dependencies
    - ensure_namespace
    - initialize_tiller
    - create_secret
    - deploy
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: http://$CI_PROJECT_PATH_SLUG-$CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN
    on_stop: stop_review
  only:
    - /^feature\/.*/

stop_review:
  stage: cleanup
  variables:
    GIT_STRATEGY: none
  script:
    - install_dependencies
    - initialize_tiller
    - delete
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  when: manual
  allow_failure: true
  only:
    - /^feature\/.*/

review prod:
  stage: review
  script:
    - install_dependencies
    - ensure_namespace
    - initialize_tiller
    - create_secret
    - deploy
  environment:
    name: review-prod
    url: http://$CI_PROJECT_PATH_SLUG-$CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN
    on_stop: stop_review_prod
  only:
    refs:
      - production

e2e pre prod:
  stage: e2e
  image: trion/ng-cli-karma
  environment:
    name: review-prod
  script:
    - cd client
    - npm ci
    - npm run e2e -- --dev-server-target=  --base-url=http://$CI_PROJECT_PATH_SLUG-$CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN
  only:
    refs:
      - production


stop_review_prod:
  stage: cleanup
  variables:
    GIT_STRATEGY: none
  script:
    - install_dependencies
    - initialize_tiller
    - delete
  environment:
    name: review-prod
    action: stop
  allow_failure: true
  only:
    refs:
      - production



deploy_master_dev:
  stage: delivery
  script:
    - install_dependencies
    - ensure_namespace
    - initialize_tiller
    - create_secret
    - deploy
  environment:
    name: dev
    url: http://$CI_PROJECT_PATH_SLUG-$CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN
  allow_failure: false
  only:
    refs:
      - master


staging:
  stage: delivery
  script:
    - install_dependencies
    - ensure_namespace
    - initialize_tiller
    - create_secret
    - deploy
  environment:
    name: staging
    url: http://$CI_PROJECT_PATH_SLUG-staging.$KUBE_INGRESS_BASE_DOMAIN
  only:
    refs:
      - production


production_manual:
  stage: delivery
  script:
    - install_dependencies
    - ensure_namespace
    - initialize_tiller
    - create_secret
    - deploy
  environment:
    name: production
    url: http://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
  only:
    refs:
      - production

# ---------------------------------------------------------------------------

.bash_scripts: &bash_scripts |
  # Auto DevOps variables and functions
  [[ "$TRACE" ]] && set -x
  auto_database_url=postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${CI_ENVIRONMENT_SLUG}-postgres:5432/${POSTGRES_DB}
  export DATABASE_URL=${DATABASE_URL-$auto_database_url}
  export CI_APPLICATION_REPOSITORY=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
  export CI_APPLICATION_TAG=$CI_COMMIT_SHA
  export CI_CONTAINER_NAME=ci_job_build_${CI_JOB_ID}
  export TILLER_NAMESPACE=$KUBE_NAMESPACE
  # Extract "MAJOR.MINOR" from CI_SERVER_VERSION and generate "MAJOR-MINOR-stable" for Security Products
  export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')

  function registry_login() {
    if [[ -n "$CI_REGISTRY_USER" ]]; then
      echo "Logging to GitLab Container Registry with CI credentials..."
      docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
      echo ""
    fi
  }

  function get_replicas() {
    track="${1:-stable}"
    percentage="${2:-100}"

    env_track=$( echo $track | tr -s  '[:lower:]'  '[:upper:]' )
    env_slug=$( echo ${CI_ENVIRONMENT_SLUG//-/_} | tr -s  '[:lower:]'  '[:upper:]' )

    if [[ "$track" == "stable" ]]; then
      # for stable track get number of replicas from `PRODUCTION_REPLICAS`
      eval new_replicas=\$${env_slug}_REPLICAS
      if [[ -z "$new_replicas" ]]; then
        new_replicas=$REPLICAS
      fi
    fi

    replicas="${new_replicas:-1}"
    replicas="$(($replicas * $percentage / 100))"

    # always return at least one replicas
    if [[ $replicas -gt 0 ]]; then
      echo "$replicas"
    else
      echo 1
    fi
  }

  function deploy() {
    track="${1-stable}"
    percentage="${2:-100}"

    # A simplified version of the environment name, suitable for inclusion in DNS, URLs,
    # Kubernetes labels, etc. Only present if environment:name is set.
    name="$CI_ENVIRONMENT_SLUG"

    replicas="1"

    # if track is different than stable,
    # re-use all attached resources
    if [[ "$track" != "stable" ]]; then
      name="$name-$track"
    fi

    replicas=$(get_replicas "$track" "$percentage")

    if [[ "$CI_PROJECT_VISIBILITY" != "public" ]]; then
      secret_name='gitlab-registry'
    else
      secret_name=''
    fi

    echo "Deploying new release..."

    helm upgrade --install \
      --set serviceServer.url="$CI_ENVIRONMENT_URL" \
      --set serviceClient.url="$CI_ENVIRONMENT_URL" \
      --set releaseOverride="$CI_ENVIRONMENT_SLUG" \
      --set imageClient.repository="$CI_APPLICATION_REPOSITORY/client" \
      --set imageClient.tag="$CI_APPLICATION_TAG" \
      --set imageServer.repository="$CI_APPLICATION_REPOSITORY/server" \
      --set imageServer.tag="$CI_APPLICATION_TAG" \
      --set application.track="$track" \
      --set application.database_url="$DATABASE_URL" \
      --set replicaCount="$replicas" \
      --set postgresql.postgresDatabase="$POSTGRES_DB" \
      --namespace="$KUBE_NAMESPACE" \
      "$name" \
      chart/

    helm list
  }

  function install_dependencies() {
    apk add -U openssl curl tar gzip bash ca-certificates git
    curl -L -o /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub
    curl -L -O https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.28-r0/glibc-2.28-r0.apk
    apk add glibc-2.28-r0.apk
    rm glibc-2.28-r0.apk

    curl "https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx
    mv linux-amd64/helm /usr/bin/
    mv linux-amd64/tiller /usr/bin/
    helm version --client
    tiller -version

    curl -L -o /usr/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl"
    chmod +x /usr/bin/kubectl
    kubectl version --client
  }

  function setup_docker() {
    if ! docker info &>/dev/null; then
      if [ -z "$DOCKER_HOST" -a "$KUBERNETES_PORT" ]; then
        export DOCKER_HOST='tcp://localhost:2375'
      fi
    fi
  }

  function ensure_namespace() {
    kubectl describe namespace "$KUBE_NAMESPACE" || kubectl create namespace "$KUBE_NAMESPACE"
  }

  function initialize_tiller() {
    echo "Checking Tiller..."

    export HELM_HOST="localhost:44134"
    tiller -listen ${HELM_HOST} -alsologtostderr > /dev/null 2>&1 &
    echo "Tiller is listening on ${HELM_HOST}"

    if ! helm version --debug; then
      echo "Failed to init Tiller."
      return 1
    fi
    echo ""
  }

  function create_secret() {
    echo "Create secret..."
    if [[ "$CI_PROJECT_VISIBILITY" == "public" ]]; then
      return
    fi

    kubectl create secret -n "$KUBE_NAMESPACE" \
      docker-registry gitlab-registry \
      --docker-server="$CI_REGISTRY" \
      --docker-username="${CI_DEPLOY_USER:-$CI_REGISTRY_USER}" \
      --docker-password="${CI_DEPLOY_PASSWORD:-$CI_REGISTRY_PASSWORD}" \
      --docker-email="$GITLAB_USER_EMAIL" \
      -o yaml --dry-run | kubectl replace -n "$KUBE_NAMESPACE" --force -f -
  }

  function delete() {
    track="${1-stable}"
    name="$CI_ENVIRONMENT_SLUG"

    if [[ "$track" != "stable" ]]; then
      name="$name-$track"
    fi

    if [[ -n "$(helm ls -q "^$name$")" ]]; then
      helm delete --purge "$name"
    fi
  }

before_script:
  - *bash_scripts
